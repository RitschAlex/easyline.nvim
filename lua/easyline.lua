local api = vim.api
local fn = vim.fn

local highlights = require('easyline.highlights')
local git = require('easyline.git')

local M = {}

local modes = {
    ["n"] = "NORMAL",
    ["no"] = "NORMAL",
    ["v"] = "VISUAL",
    ["V"] = "VISUAL LINE",
    ["^V"] = "VISUAL BLOCK",
    ["s"] = "SELECT",
    ["S"] = "SELECT LINE",
    ["^S"] = "SELECT BLOCK",
    ["i"] = "INSERT",
    ["ic"] = "INSERT",
    ["R"] = "REPLACE",
    ["Rv"] = "VISUAL REPLACE",
    ["c"] = "COMMAND",
    ["cv"] = "VIM EX",
    ["ce"] = "EX",
    ["r"] = "PROMPT",
    ["rm"] = "MOAR",
    ["r?"] = "CONFIRM",
    ["!"] = "SHELL",
    ["t"] = "TERMINAL"
}

local function mode()
    local current_mode = api.nvim_get_mode().mode
    return string.format(" %s ", modes[current_mode]):upper()
end

local mode_colors = {
    n = "%#StatusLineAccent#",
    i = "%#StatusLineInsertAccent#",
    ic = "%#StatusLineInsertAccent#",
    v = "%#StatusLineVisualAccent#",
    V = "%#StatusLineVisualAccent#",
    ["^V"] = "%#StatusLineVisualAccent#",
    R = "%#StatusLineReplaceAccent#",
    c = "%#StatusLineCmdLineAccent#",
    t = "%#StatusLineTerminalAccent#"
}

local function update_mode_colors()
    local current_mode = api.nvim_get_mode().mode
    return mode_colors[current_mode] or "%#StatusLineAccent#"
end

local function filepath()
    local fpath = fn.fnamemodify(fn.expand("%"), ":~:.:h")
    return fpath == " " or fpath == "." and " " or
               string.format(" %%<%s/", fpath)
end

local function filename()
    local fname = fn.expand("%:t")
    return fname == "" and "" or fname .. " "
end

local function filetype() return string.format(" %s ", vim.bo.filetype):upper() end

local function lineinfo() return
    vim.bo.filetype == "alpha" and "" or "%P %l:%c " end

local function git_info()
    local branch = git.get_git_branch()
    if branch == "" then
        return ''
    else
        return "  " .. branch .. " "
    end
end

local function lsp()
    local diagnostics = {"error", "warn", "hint", "info"}
    local symbols = {error = "E", warn = "W", hint = "H", info = "I"}
    local result = {}

    for _, diag in ipairs(diagnostics) do
        local count = vim.tbl_count(vim.diagnostic.get(0, {
            severity = diag:upper()
        }))
        if count > 0 then
            table.insert(result,
                         string.format("%%#LspDiagnosticsSign%s# %s:%d ",
                                       diag:sub(1, 1):upper() .. diag:sub(2),
                                       symbols[diag], count))
        end
    end
    return "%=" .. table.concat(result, "")
end

local function merge_with_defaults(defaults, user_config)
    if user_config == nil then return defaults end

    local merged_config = {}
    for key, value in pairs(defaults) do
        merged_config[key] = user_config[key] or value
    end

    return merged_config
end

local function active_statusline()
    return table.concat({
        update_mode_colors(), mode(), "%#StatusLineGit#", git_info(),
        "%#StatusLineFile#", filepath(), filename(), "%#StatusLineFile#", lsp(),
        "%#StatusLineExtra#", filetype(), lineinfo()
    })
end

local function short_statusline() return "%#StatusLineNC#   NvimTree" end

local function inactive_statusline() return " %F " end

local function setup_autocommands()
    vim.cmd([[
        augroup EasyLine
            au!
            au VimEnter,WinEnter,BufEnter * setlocal statusline=%!v:lua.require('easyline').active()
            au WinLeave,BufLeave * setlocal statusline=%!v:lua.require('easyline').inactive()
            au WinEnter,BufEnter,FileType NvimTree, CHADTree  setlocal statusline=%!v:lua.require('easyline').short()
        augroup END
    ]])
end

function M.setup(config)
    config = config or {}
    local hl = merge_with_defaults(highlights.defaults, config.highlights)
    highlights.set_highlights(hl)
    setup_autocommands()
end

M.active = active_statusline
M.inactive = inactive_statusline
M.short = short_statusline

return M
