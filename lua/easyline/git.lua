local M = {}

M.git_branch_cache = {branch = nil, path = nil}

local function is_windows()
    return vim.fn.has('win32') == 1 or vim.fn.has('win64') == 1
end

local function get_git_branch_windows()
    local status_ok, _ = pcall(require, 'gitsigns')
    if not status_ok then return " " end

    if vim.b.gitsigns_head then return vim.trim(vim.b.gitsigns_head) end

    local git_command = 'git rev-parse --abbrev-ref HEAD 2> nul'
    local check_git_repo_command = 'git rev-parse --is-inside-work-tree 2> nul'

    local is_git_repo = vim.fn.system(check_git_repo_command)
    if vim.trim(is_git_repo) ~= 'true' then return '' end

    local branch = vim.fn.system(git_command)
    return branch ~= "" and vim.trim(branch) or ""
end

local function get_git_branch_non_windows()
    local git_command = 'git rev-parse --abbrev-ref HEAD 2> /dev/null'
    local check_git_repo_command = 'git rev-parse --is-inside-work-tree'

    local is_git_repo = vim.fn.system(check_git_repo_command)
    if vim.trim(is_git_repo) ~= 'true' then return '' end

    local branch = vim.fn.system(git_command)
    return branch ~= "" and "" .. vim.trim(branch) or ""
end

local function update_git_branch_cache()
    local current_path = vim.fn.getcwd()

    -- Only update cache if the path has changed
    if M.git_branch_cache.path ~= current_path then
        local git_branch
        if is_windows() then
            git_branch = get_git_branch_windows()
        else
            git_branch = get_git_branch_non_windows()
        end

        M.git_branch_cache = {branch = git_branch, path = current_path}
    end
end

function M.get_git_branch()
    update_git_branch_cache()
    return M.git_branch_cache.branch
end

return M
