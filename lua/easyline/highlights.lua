local cmd = vim.cmd
local highlight = vim.api.nvim_set_hl

local palette = {
    khaki = "#c2c292",
    yellow = "#e3c78a",
    orange = "#de935f",
    coral = "#f09479",
    orchid = "#e196a2",
    lime = "#85dc85",
    green = "#8cc85f",
    emerald = "#36c692",
    blue = "#80a0ff",
    sky = "#74b2ff",
    turquoise = "#79dac8",
    purple = "#ae81ff",
    cranberry = "#e65e72",
    violet = "#cf87e8",
    crimson = "#ff5189",
    red = "#ff5454",
    spring = "#00875f",
    mineral = "#314940",
    bay = "#4d5d8d",
    white = "#e4e4e4",
    black = "#080808",
    grey = "#9ca1aa"
}

local M = {}

M.defaults = {
    StatusLine = {fg = palette.white, bg = palette.black},
    StatusLineAccent = {fg = palette.black, bg = palette.green},
    StatusLineInsertAccent = {fg = palette.black, bg = palette.blue},
    StatusLineVisualAccent = {fg = palette.black, bg = palette.violet},
    StatusLineReplaceAccent = {fg = palette.black, bg = palette.turquoise},
    StatusLineCmdLineAccent = {fg = palette.black, bg = palette.red},
    StatusLineTerminalAccent = {fg = palette.black, bg = palette.red},
    StatusLineExtra = {fg = palette.black, bg = palette.blue},
    StatusLineFile = {fg = palette.white, bg = palette.black},
    StatusLineNC = {fg = palette.black, bg = palette.black},
    StatusLineGit = {fg = palette.black, bg = palette.grey},
    LspDiagnosticsSignError = {fg = palette.black, bg = "#ee5396"},
    LspDiagnosticsSignWarn = {fg = palette.black, bg = "#be95ff"},
    LspDiagnosticsSignHint = {fg = palette.black, bg = "#3ddbd9"},
    LspDiagnosticsSignInfo = {fg = palette.black, bg = "#78a9ff"}
}

function M.set_highlights(highlights)
    for group, colors in pairs(highlights) do
        local fg = colors.fg and ("guifg=" .. colors.fg) or "guifg=NONE"
        local bg = colors.bg and ("guibg=" .. colors.bg) or "guibg=NONE"
        -- cmd("hi " .. group .. " " .. fg .. " " .. bg)
        highlight(0, group, {bg = colors.bg, fg = colors.fg})
    end
end

return M
